import matplotlib.pyplot as plt
import numpy
import random
import heapq


M = 1000               # total money
N = 100                # number of traders
trade_part = 0.1       # part of trader's capital to trade
market_price = 1.0     # initial stock market price

cycles_max = 1000      # number of trading cycles
scale = 0.1         # numpy.random.lognormal parameter

stocks_pool = [0.0] * N
money_pool  = [0.0] * N

price_log = []


indexes = [i for i in range(0, N)]
for i in indexes:
    if (i < N/2): money_pool[i]  = float(N) / M
    else:         stocks_pool[i] = float(N) / M

cycle = 0
while (cycle < cycles_max):
    cycle += 1

    sellers_heap = [] # min heap
    buyers_heap  = [] # max heap or (-min) heap (heapq is only minPQ)
    random.shuffle(indexes)
    for i1 in indexes:
        price1 = market_price * numpy.random.normal(loc=1.0, scale=scale)
        
        if (money_pool[i1] < stocks_pool[i1]): # try to sell
            vol1 = trade_part * stocks_pool[i1]
            price_coeff = -1.0
            heap1 = sellers_heap
            heap2 = buyers_heap
        elif (money_pool[i1] > stocks_pool[i1]): # try to buy
            vol1 = trade_part * money_pool[i1]
            price_coeff = 1.0
            heap1 = buyers_heap
            heap2 = sellers_heap
        else: continue

        while (True):
            if (vol1 <= 0): break
            if (len(heap2) == 0):
                heapq.heappush(heap1, (-price_coeff * price1, vol1, i1))
                break
            if (price_coeff * price1 < heap2[0][0]):
                heapq.heappush(heap1, (-price_coeff * price1, vol1, i1))
                break
                
            price2, vol2, i2 = heapq.heappop(heap2)
            price2 *= price_coeff

            deal_vol = min(vol1, vol2)

            vol1 -= deal_vol
            vol1 = max(vol1, 0.0)
            vol2 -= deal_vol
            vol2 = max(vol2, 0.0)

            money_pool[i1]  -= price_coeff * price2 * deal_vol
            stocks_pool[i1] += price_coeff * price2 * deal_vol
                
            money_pool[i2]  += price_coeff * price2 * deal_vol
            stocks_pool[i2] -= price_coeff * price2 * deal_vol
                    
            if (vol2 > 0): heapq.heappush(heap2, (price_coeff * price2, vol2, i2))

            market_price = price2

    price_log.append(market_price)


plt.plot(price_log)
plt.show()
